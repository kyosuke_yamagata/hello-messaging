package com.example.hellosink;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.OutputCaptureRule;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.hellosink.HelloSinkApplication.Tweet;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class HelloSinkApplicationTests {
	@Autowired
	Sink sink;
	@Rule
	public OutputCaptureRule capture = new OutputCaptureRule();

	@Test
	public void testPrint() {
		Tweet tweet = new Tweet();
		tweet.tweet = "Hello";
		
		sink.input().send(MessageBuilder.withPayload(tweet).build());

		//captureのDocあり　https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/test/system/OutputCaptureRule.html
		capture.expect(containsString("Received Hello"));
	}

}