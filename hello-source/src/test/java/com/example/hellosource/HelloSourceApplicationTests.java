package com.example.hellosource;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.messaging.Message;
import org.springframework.test.context.junit4.SpringRunner;
import com.example.hellosource.HelloSourceApplication.Tweet;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class HelloSourceApplicationTests {

	@Autowired
	HelloSourceApplication app;
	@Autowired
	MessageCollector collector;
	@Autowired
	Source source;

	@Test
	@SuppressWarnings("unchecked")
	public void testTweet() {
		Tweet tweet = new Tweet();
		tweet.tweet = "hello!";
		app.tweet(tweet);

		Message<String> message = (Message<String>) collector.forChannel(source.output())
				.poll();

		assertThat(message.getPayload()).isInstanceOf(String.class);
		assertThat(message.getPayload()).isEqualTo("{\"tweet\":\"hello!\"}");
		assertThat(message.getHeaders().get("contentType").toString())
				.isEqualTo("application/json");
	}

}