package com.example.helloprocessor;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.test.binder.MessageCollector;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.helloprocessor.HelloProcessorApplication.Tweet;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)

class HelloProcessorApplicationTests {
	@Autowired
	HelloProcessorApplication app;	
	@Autowired
	MessageCollector collector;
	@Autowired
	Processor processor;

	@SuppressWarnings("unchecked")
	@Test
	void testProcessing() {
		Tweet tweet = new Tweet();
		tweet.tweet = "hello!";
		
		processor.input().send(MessageBuilder.withPayload(tweet).build());
		
		Message<String> message = (Message<String>) collector.forChannel(processor.output())
				.poll();

		assertThat(message.getPayload()).isInstanceOf(String.class);
		assertThat(message.getPayload()).isEqualTo("{\"tweet\":\"hello! processing!\"}");
		assertThat(message.getHeaders().get("contentType").toString())
				.isEqualTo("application/json");

	}

}
