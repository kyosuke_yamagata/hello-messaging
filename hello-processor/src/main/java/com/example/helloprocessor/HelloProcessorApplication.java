package com.example.helloprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.handler.annotation.SendTo;

@SpringBootApplication
@EnableBinding(Processor.class)
public class HelloProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloProcessorApplication.class, args);
	}

	@StreamListener(Processor.INPUT)
    @SendTo(Processor.OUTPUT)
	public Tweet transform(Tweet tweet) {
		tweet.tweet += " processing!";
		return tweet;
	}
	
	public static class Tweet {
		public String tweet;
	}
}
